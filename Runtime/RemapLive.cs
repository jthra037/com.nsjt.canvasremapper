﻿// ========================================================================================
// Canvas Remapper - Map one Canvas onto another using the CanvasScaler and RectTransforms
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using UnityEngine;

[RequireComponent(typeof(Canvas))]
public class RemapLive : MonoBehaviour
{
	private RectTransform[] rectTransforms;
	private float inverseScaler;

	[SerializeField]
	private Canvas target;
	private RectTransform targetRect;
	private Canvas self;
	private RectTransform selfRect;

	private float xRefDelta;
	private float yRefDelta;

	void Start()
	{
		rectTransforms = getChildRectTransforms();
		self = GetComponent<Canvas>();
		selfRect = GetComponent<RectTransform>();
		targetRect = target.GetComponent<RectTransform>();

		xRefDelta = selfRect.sizeDelta.x - targetRect.sizeDelta.x;
		yRefDelta = selfRect.sizeDelta.y - targetRect.sizeDelta.y;

		inverseScaler = getInverseScaler(target);
		// fix most sizing problems by rescaling the 1st gen children
		scaleRects(inverseScaler);
		// fix position by inversescaler
		scaleAnchorPositions(inverseScaler);
		// correct for differences in aspect ratio
		scaleSizeDelta();
	}

	private void scaleSizeDelta()
	{
		foreach (var rect in rectTransforms)
		{
			var x = (rect.anchorMin.x - rect.anchorMax.x) * xRefDelta;
			var y = (rect.anchorMin.y - rect.anchorMax.y) * yRefDelta;
			rect.sizeDelta += new Vector2(x, y);
		}
	}

	private void scaleAnchorPositions(float scaler)
	{
		foreach(var rect in rectTransforms)
		{
			rect.anchoredPosition = rect.anchoredPosition * scaler;
		}
	}

	private RectTransform[] getChildRectTransforms()
	{
		RectTransform[] childTransforms = new RectTransform[transform.childCount];
		for(int i = 0; i < transform.childCount; i++)
		{
			var child = transform.GetChild(i);
			childTransforms[i] = child.GetComponent<RectTransform>();
		}

		return childTransforms;
	}

	private void scaleRects(float scaler)
	{
		foreach(var rect in rectTransforms)
		{
			rect.localScale *= scaler;
		}
	}

	private float getInverseScaler(Canvas target)
	{
		return target.scaleFactor / self.scaleFactor;
	}
}
