﻿// ========================================================================================
// Canvas Remapper - Map one Canvas onto another using the CanvasScaler and RectTransforms
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CanvasRemapper : IDisposable
{
	private GameObject original;
	private Canvas target;
	private RectTransform targetRect;
	private RectTransform[] originalRectTransforms;
	private LayoutElement[] originalLayoutElements;
	private LayoutGroup[] originalLayoutGroups;
	private Text[] originalUnityText;
	private TextMeshProUGUI[] originalTMPText;

	public CanvasRemapper(GameObject original)
	{
		this.original = original;
		target = original.GetComponent<Canvas>();
		targetRect = original.GetComponent<RectTransform>();
		originalRectTransforms = original.GetComponentsInChildren<RectTransform>(true);
		originalLayoutElements = original.GetComponentsInChildren<LayoutElement>(true);
		originalLayoutGroups = original.GetComponentsInChildren<LayoutGroup>(true);
		originalUnityText = original.GetComponentsInChildren<Text>(true);
		originalTMPText = original.GetComponentsInChildren<TextMeshProUGUI>(true);
	}

	public void RemapExisting(GameObject existing)
	{
		var remapped = existing;
		var self = remapped.GetComponent<Canvas>();
		var selfRect = remapped.GetComponent<RectTransform>();
		var rectTransforms = remapped.GetComponentsInChildren<RectTransform>(true);
		var layoutElements = remapped.GetComponentsInChildren<LayoutElement>(true);
		var layoutGroups = remapped.GetComponentsInChildren<LayoutGroup>(true);
		var unityText = remapped.GetComponentsInChildren<Text>(true);
		var TMPText = remapped.GetComponentsInChildren<TextMeshProUGUI>(true);

		var inverseScaler = getInverseScaler(target, self);
		var xRefDelta = selfRect.sizeDelta.x - targetRect.sizeDelta.x;
		var yRefDelta = selfRect.sizeDelta.y - targetRect.sizeDelta.y;

		scaleAnchorPositions(rectTransforms, inverseScaler);
		scaleSizeDelta(rectTransforms, xRefDelta, yRefDelta, inverseScaler);
		scaleLayoutElements(layoutElements, originalLayoutElements, inverseScaler);
		scaleLayoutGroups(layoutGroups, originalLayoutGroups, inverseScaler);
		scaleUnityText(unityText, originalUnityText, inverseScaler);
		scaleTMPText(TMPText, originalTMPText, inverseScaler);

	}

	private void scaleTMPText(TextMeshProUGUI[] TMPText, TextMeshProUGUI[] originalTMPText, float inverseScaler)
	{
		for (int i = 0; i < TMPText.Length; i++)
		{
			TextMeshProUGUI text = TMPText[i];
			TextMeshProUGUI original = originalTMPText[i];

			text.fontSize = Mathf.FloorToInt(original.fontSize * inverseScaler);
			text.fontSizeMin = Mathf.FloorToInt(original.fontSizeMin * inverseScaler);
			text.fontSizeMax = Mathf.FloorToInt(original.fontSizeMax * inverseScaler);
		}
	}

	private void scaleUnityText(Text[] unityText, Text[] originalUnityText, float inverseScaler)
	{
		for (int i = 0; i < unityText.Length; i++)
		{
			Text text = unityText[i];
			Text original = originalUnityText[i];

			text.fontSize = Mathf.RoundToInt(original.fontSize * inverseScaler);
			text.resizeTextMinSize = Mathf.RoundToInt(original.resizeTextMinSize * inverseScaler);
			text.resizeTextMaxSize = Mathf.RoundToInt(original.resizeTextMaxSize * inverseScaler);
		}
	}

	public GameObject GetRemapped()
	{
		var remapped = UnityEngine.Object.Instantiate(original);
		RemapExisting(remapped);

		return remapped;
	}

	private void scaleLayoutGroups(LayoutGroup[] layoutGroups, LayoutGroup[] originalLayoutGroups, float scaler)
	{
		for (int i = 0; i < layoutGroups.Length; i++)
		{
			LayoutGroup group = layoutGroups[i];
			LayoutGroup original = originalLayoutGroups[i];

			group.padding = scaleRectOffset(original.padding, scaler);

			switch(group)
			{
				case HorizontalOrVerticalLayoutGroup inner:
					var originalHV = (HorizontalOrVerticalLayoutGroup)original;
					scaleHVGroup(inner, originalHV, scaler);
					break;
				case GridLayoutGroup inner:
					var originalGrid = (GridLayoutGroup)original;
					scaleGridGroup(inner, originalGrid, scaler);
					break;
				default:
					Debug.LogWarning($"{group.name}::{group.GetType().Name} was passed in for remapping, but is not yet supported.");
					break;
			}
		}
	}

	private void scaleHVGroup(HorizontalOrVerticalLayoutGroup group, HorizontalOrVerticalLayoutGroup original, float scaler)
	{
		group.spacing = original.spacing * scaler;
	}

	private void scaleGridGroup(GridLayoutGroup group, GridLayoutGroup original, float scaler)
	{
		group.spacing = original.spacing * scaler;
		group.cellSize = original.cellSize * scaler;
	}

	private RectOffset scaleRectOffset(RectOffset padding, float scaler)
	{
		return new RectOffset(Mathf.FloorToInt(padding.left * scaler),
			Mathf.FloorToInt(padding.right * scaler),
			Mathf.FloorToInt(padding.top * scaler),
			Mathf.FloorToInt(padding.bottom * scaler));
	}

	private void scaleLayoutElements(LayoutElement[] layoutElements, LayoutElement[] originalLayoutElements, float scaler)
	{
		for (int i = 0; i < layoutElements.Length; i++)
		{
			LayoutElement element = layoutElements[i];
			LayoutElement original = originalLayoutElements[i];

			element.minWidth = element.minWidth == -1 ?
				element.minWidth :
				original.minWidth * scaler;
			element.minHeight = element.minHeight == -1 ?
				element.minHeight :
				original.minHeight * scaler;
			element.preferredWidth = element.preferredWidth == -1 ?
				element.preferredWidth :
				original.preferredWidth * scaler;
			element.preferredHeight = element.preferredHeight == -1 ?
				element.preferredHeight :
				original.preferredHeight * scaler;
			element.flexibleWidth = element.flexibleWidth == -1 ?
				element.flexibleWidth :
				original.flexibleWidth * scaler;
			element.flexibleHeight = element.flexibleHeight == -1 ?
				element.flexibleHeight :
				original.flexibleHeight * scaler;
		}
	}

	private void copyScaler(ref CanvasScaler selfScaler, CanvasScaler scaler)
	{
		selfScaler.scaleFactor = scaler.scaleFactor;
		selfScaler.referenceResolution = scaler.referenceResolution;
		selfScaler.screenMatchMode = scaler.screenMatchMode;
		selfScaler.matchWidthOrHeight = scaler.matchWidthOrHeight;
		selfScaler.defaultSpriteDPI = scaler.defaultSpriteDPI;
		selfScaler.fallbackScreenDPI = scaler.fallbackScreenDPI;
		selfScaler.uiScaleMode = scaler.uiScaleMode;
		selfScaler.dynamicPixelsPerUnit = scaler.dynamicPixelsPerUnit;
		selfScaler.physicalUnit = scaler.physicalUnit;
		selfScaler.referencePixelsPerUnit = scaler.referencePixelsPerUnit;
	}

	private void scaleSizeDelta(RectTransform[] rectTransforms, float xRefDelta, float yRefDelta, float inverseScaler)
	{
		for (int i = 0; i < rectTransforms.Length; i++)
		{
			RectTransform rect = rectTransforms[i];
			RectTransform originalRect = originalRectTransforms[i];
			rect.sizeDelta = originalRect.sizeDelta * inverseScaler;
		}
	}

	private void scaleAnchorPositions(RectTransform[] rectTransforms, float scaler)
	{
		for (int i = 0; i < rectTransforms.Length; i++)
		{
			RectTransform rect = rectTransforms[i];
			RectTransform originalRect = originalRectTransforms[i];
			rect.anchoredPosition = originalRect.anchoredPosition * scaler;
		}
	}

	private RectTransform[] getChildRectTransforms(Transform transform)
	{
		RectTransform[] childTransforms = new RectTransform[transform.childCount];
		for (int i = 0; i < transform.childCount; i++)
		{
			var child = transform.GetChild(i);
			childTransforms[i] = child.GetComponent<RectTransform>();
		}

		return childTransforms;
	}

	private void scaleRects(RectTransform[] rectTransforms, float scaler)
	{
		Vector3 scale = Vector3.one * scaler;
		foreach (var rect in rectTransforms)
		{
			rect.localScale = scale;
		}
	}

	private float getInverseScaler(Canvas target, Canvas self)
	{
		return target.scaleFactor / self.scaleFactor;
	}

	public void Dispose()
	{

	}
}
