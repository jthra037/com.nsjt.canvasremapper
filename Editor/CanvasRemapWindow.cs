﻿// ========================================================================================
// Canvas Remapper - Map one Canvas onto another using the CanvasScaler and RectTransforms
// ========================================================================================
// 2021, Jacob Thrasher / https://gitlab.com/jthra037 / http://twitter.com/NSJacob1
// ========================================================================================

using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class CanvasRemapWindow : EditorWindow
{
	private Canvas target;
	private Canvas remapped;

	private string msg = "";

	[MenuItem("Tools/Canvas Remapper")]
	static void Init()
	{
		CanvasRemapWindow window = GetWindow<CanvasRemapWindow>();
		window.Show();
	}

	private void OnGUI()
	{
		GUILayout.Label("Canvas Remapper", EditorStyles.boldLabel);
		target = EditorGUILayout.ObjectField("Original Root Canvas", target, typeof(Canvas), true) as Canvas;
		remapped = EditorGUILayout.ObjectField("Canvas to Remap", remapped, typeof(Canvas), true) as Canvas;

		if (remapped == null)
			return;

		var prefabPath = PrefabUtility.GetPrefabAssetPathOfNearestInstanceRoot(remapped.gameObject);
		if (!string.IsNullOrEmpty(prefabPath))
		{
			drawButton(prefabPath);
		}
		else
		{
			msg = $"Could not find guid for {remapped.name}. Are you using a Canvas that has a prefab?";
		}

		GUILayout.Label(msg);
	}

	private void drawButton(string prefabPath)
	{
		if (GUILayout.Button("Remap Canvas"))
		{
			remapExisting(remapped.gameObject);
			PrefabUtility.ApplyPrefabInstance(remapped.gameObject, InteractionMode.UserAction);
			AssetDatabase.SaveAssets();
			msg = $"{remapped.name} was remapped and updated";
			target = null;
			remapped = null;
		}
		else
		{
			msg = "";
		}
	}

	private void remapExisting(GameObject toRemap)
	{
		var remapper = new CanvasRemapper(target.gameObject);
		remapper.RemapExisting(toRemap);
	}
}
